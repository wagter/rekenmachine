package wagtercalc.model;

import wagtercalc.CustomButton;

import java.util.HashMap;

/**
 * The number buttons grid model.
 * 
 * @author Joris Wagter
 */
public class NumberButtonsModel extends AbstractButtonsModel
{
	@Override
	public HashMap<Character, CustomButton> createButtonMap()
	{
		HashMap<Character, CustomButton> map = new HashMap<Character, CustomButton>();
		
		map.put( '1', new CustomButton('1') );
		map.put( '2', new CustomButton('2') );
		map.put( '3', new CustomButton('3') );
		map.put( '4', new CustomButton('4') );
		map.put( '5', new CustomButton('5') );
		map.put( '6', new CustomButton('6') );
		map.put( '7', new CustomButton('7') );
		map.put( '8', new CustomButton('8') );
		map.put( '9', new CustomButton('9') );
		map.put( '0', new CustomButton('0') );
		map.put( ',', new CustomButton(',') );
		map.put( '(', new CustomButton(' ') );
		
		return map;
	}
}
