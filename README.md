# Eenvoudige rekenmachine 
Hallo, ik ben Joris. Dit is de README van de rekenmachine die ik gemaakt heb (aan het maken ben). De aanleiding van dit project is een schoolopdracht.

## Gebruik
De rekenmachine kan expressies uitrekenen zoals "1 + 2 * 3 - 5 / 3". 
Hij houdt daarbij rekening met de goede volgorde van de operators. De haakjes, "(" en ")" worden misschien nog toegevoegd. Voor decimale getallen wordt de ',' gebruikt. Het antwoord wordt afgerond op 1 decimaal, dat staat in de opdracht.
Je kunt de rekenmachine bedienen met de knopjes op het scherm. Je kunt in plaats van de knopjes op het scherm, ook de knopjes op je toetsenbord gebruiken. De karakters (ook voor de functies) zijn hetzelfde als de knopjes op je scherm.

## Code
Deze applicatie is gemaakt met Java Swing volgens het MVC principe zoals beschreven in hoofstuk 9 van het boek "Aan de slag met Java".

### Knopjes
De standaard knopjes van Java Swing, de JButton voldoet niet aan mijn eisen. Ze zien er lelijk uit, en zijn niet makkelijk te stijlen.

>### De oplossing
>Om zelf een knopje te tekenen heb ik de klasse CustomButton gemaakt. Deze klasse overerft van JPanel. Iedere knop heeft 1 karakter. Iedere knop kan de staat "hover" hebben. Deze staat bepaalt de kleur van de knop. Deze staat wordt veranderd door de Controller als de gebruiker met de muis boven de knop zweeft.
 

### Scheiding Controller en View
In het hierboven genoemde boek staat een voorbeeld van een MVC applicatie waarin de Controller een JPanel overerft. De knoppen worden direct aan het paneel van de Controller toegevoegd, die weer direct in het hoofdpaneel zit. Naar mijn idee is een JPanel een View klasse. In het voorbeeld zitten 
maar 3 knoppen, dan blijft het simpel. Deze rekenmachine bevat 18 knoppen. Ik wil de knoppen plaatsen in een grid gemaakt van verticale en horizontale BoxLayouts. Daarvoor moet ik dus complexe 
layouts gaan bouwen. Daar vind ik een Controller niet de juiste plaats voor. Omdat dit dingen zijn die overduidelijk met lay-out te maken hebben moeten ze gedaan worden in een View klasse.

>### De oplossing
>Om dit probleem op te lossen had ik 1 Model en 1 View klasse voor de knoppen kunnen maken. Ik wil in deze applicatie graag een paar mogelijkheiden van
>overerving te demonstreren. Daarom hheb ik er voor gekozen om te werken met twee abstracte klassen, voor de Model en de View van knoppen. Vier andere klassen oververven deze klasse.
>Dat zijn de klassen van de knoppen voor de nummers en de klassen van de knoppen voor de operators.

>Op deze manier kan ik zeker weten welke set knoppen het is (nummers of operators), maar ze toch gelijk behandelen op verschillende plekken. 
>Ook is het makkelijk uit te breiden met eventuele aanvullende sets met knoppen in hun eigen layouts.