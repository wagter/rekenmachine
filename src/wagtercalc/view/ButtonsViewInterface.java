package wagtercalc.view;

/**
 * Interface to be sure children of AbstractButtonsView have the method "initLayout()" implemented.
 * 
 * @author Joris Wagter
 *
 */
public interface ButtonsViewInterface 
{
	public void initLayout();
}
