package wagtercalc.model;

import wagtercalc.CustomButton;

import java.util.HashMap;

/**
 * @author Joris Wagter
 */
public class OperatorButtonsModel extends AbstractButtonsModel
{
	@Override
	public HashMap<Character, CustomButton> createButtonMap()
	{
		HashMap<Character, CustomButton> map = new HashMap<Character, CustomButton>();
		
		map.put( '+', new CustomButton('+') );
		map.put( '-', new CustomButton('-') );
		map.put( '*', new CustomButton('*') );
		map.put( '/', new CustomButton('/') );
		map.put( 'c', new CustomButton('c') );
		map.put( 'd', new CustomButton('d') );
		map.put( '=', new CustomButton('=') );
		map.put( ')', new CustomButton(' ') );
		
		return map;
	}
}
