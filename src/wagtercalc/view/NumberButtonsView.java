package wagtercalc.view;

import wagtercalc.model.NumberButtonsModel;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class NumberButtonsView extends AbstractButtonsView 
{
	private static final long serialVersionUID = 1L;

	/**
	 * NumberButtonsView class constructor.
	 * 
	 * @param model a NumberButtonsModel instance.
	 */
	public NumberButtonsView( NumberButtonsModel model ) 
	{
		super(model);
	}
	
	@Override
	public void initLayout()
	{
		add( createCol1() );
		add( Box.createRigidArea( hSpacer ) );
		add( createCol2() );
		add( Box.createRigidArea( hSpacer ) );
		add( createCol3() );
	}
	
	/**
	 * Create the first button column.
	 * 
	 * @return the first column panel.
	 */
	private JPanel createCol1()
	{
		JPanel col = new JPanel();
		col.setLayout( new BoxLayout(col, BoxLayout.Y_AXIS) );
		
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton('7') );
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton('4') );
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton('1') );
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton(',') );
		col.add( Box.createRigidArea( vSpacer ) );
		
		return col;
	}
	
	/**
	 * Create the second button column.
	 * 
	 * @return the second column panel.
	 */
	private JPanel createCol2()
	{
		JPanel col = new JPanel();
		col.setLayout( new BoxLayout(col, BoxLayout.Y_AXIS) );
		
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton('8') );
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton('5') );
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton('2') );
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton('0') );
		col.add( Box.createRigidArea( vSpacer ) );
		
		return col;
	}
	
	/**
	 * Create the third button column.
	 * 
	 * @return the third button column.
	 */
	private JPanel createCol3()
	{
		JPanel col = new JPanel();
		col.setLayout( new BoxLayout(col, BoxLayout.Y_AXIS) );
		
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton('9') );
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton('6') );
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton('3') );
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton('(') );
		col.add( Box.createRigidArea( vSpacer ) );
		
		return col;
	}
	

}
