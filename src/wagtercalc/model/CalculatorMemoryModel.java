package wagtercalc.model;

import java.util.ArrayList;
import java.util.HashMap;

public class CalculatorMemoryModel 
{
	private ArrayList<HashMap<Integer, String>> memoryList;
	
	private int currentIndex = 0;
	
	public CalculatorMemoryModel()
	{
		memoryList = new ArrayList<HashMap<Integer, String>>();
		memoryList.add( new HashMap<Integer, String>() );
	}
	
	public HashMap<Integer, String> get( int index )
	{
		return memoryList.get( index );
	}
	
	public HashMap<Integer, String> getCurrent( )
	{
		return get( currentIndex );
	}
	
	public HashMap<Integer, String> next()
	{
		if ( memoryList.size() < currentIndex + 1) {
			currentIndex++;
		}
		return get( currentIndex );
	}
	
	public HashMap<Integer, String> prev()
	{
		if ( 0 < currentIndex - 1) {
			currentIndex--;
		}
		return get( currentIndex );
	}
	
	public HashMap<Integer, String> first()
	{
		currentIndex = 0;
		return get( currentIndex );
	}
	
	public HashMap<Integer, String> last()
	{
		currentIndex = 0;
		return get( currentIndex );
	}
	
	
	
	
}
