/**
 * 
 */
package wagtercalc.model;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Joris Wagter
 */
public class CalculatorModel 
{
	/**
	 * The HashMap containing the expression.
	 */
	private HashMap<Integer, String> expressionMap;
	
	/**
	 * The answer
	 */
	private String answer;
	
	/**
	 * CalculatorModel class constructor
	 */
	public CalculatorModel()
	{	
		expressionMap = new HashMap<Integer, String>();
		expressionMap.put(0, "0");
		
		answer = null;
	}
	
	/**
	 * CalculatorModel class constructor
	 * 
	 * @param lastAnswer the answer to start a new model with.
	 */
	public CalculatorModel( String lastAnswer )
	{
		this();
		expressionMap.put( 0, "0" + lastAnswer );
	}
	
	/**
	 * CalculatorModel class constructor
	 * 
	 * @param expressionMap the expressionMap to create a new model with.
	 * @param answer the answer to create a new model with.
	 */
	public CalculatorModel( HashMap<Integer, String> expressionMap, String answer )
	{
		this();
		this.expressionMap = expressionMap;
		this.answer = answer;
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		
		for ( Map.Entry<Integer, String> entry : expressionMap.entrySet() )
		   	sb.append( entry.getValue() + " ");
		
		if ( answer != null )
			sb.append( "= " + answer );

		return sb.toString();
	}
	
	/**
	 * Get a string with spaces removed.
	 * 
	 * @return the compact string.
	 */
	public String toCompactString()
	{
		return toString().replaceAll( "\\s+","" );
	}
	
	/**
	 * Get the expression map.
	 * 
	 * @return the expression map.
	 */
	public HashMap<Integer, String> getExpressionMap()
	{
		return expressionMap;
	}
	
	/**
	 * Set the expression map.
	 * 
	 * @param expressionMap the new expression map.
	 */
	public void setExpressionMap( HashMap<Integer, String> expressionMap )
	{
		this.expressionMap = expressionMap;
	}
	
	/**
	 * Get the answer.
	 * 
	 * @return the answer..
	 */
	public String getAnswer()
	{
		return answer;
	}
	
	/**
	 * Set the answer.
	 * 
	 * @param answer the answer.
	 */
	public void setAnswer( String answer )
	{
		this.answer = answer;
	}
	
	

}
