package wagtercalc.model;

import wagtercalc.CustomButton;

import java.util.HashMap;

public abstract class AbstractButtonsModel implements ButtonsModelInterface
{
	/**
	 * The HashMap containing the buttons.
	 */
	protected HashMap<Character, CustomButton> buttonMap = createButtonMap();

	/**
	 * Create the HashMap containing the buttons.
	 * 
	 * @return the HashMap containing the buttons.
	 */
	public HashMap<Character, CustomButton> createButtonMap()
	{	
		return new HashMap<Character, CustomButton>();
	}
	
	/**
	 * Get the HashMap containing the buttons.
	 * 
	 * @return the HashMap containing the buttons.
	 */
	public HashMap<Character, CustomButton> getButtonMap()
	{
		return buttonMap;
	}
	
	
	public CustomButton getButton( char c )
	{
		return buttonMap.get( c );
	}
}
