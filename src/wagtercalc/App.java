/**
 * 
 */
package wagtercalc;

import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;

import wagtercalc.view.CalculatorView;
import wagtercalc.model.CalculatorModel;
import wagtercalc.controller.CalculatorController;

import wagtercalc.model.NumberButtonsModel;
import wagtercalc.view.NumberButtonsView;

import wagtercalc.view.OperatorButtonsView;
import wagtercalc.model.OperatorButtonsModel;

/**
 * @author Joris Wagter
 */
public class App 
{
	
	/**
	 * The startup method for the calculator application.
	 * 
	 * @param args arguments.
	 */
	public static void main( String[] args ) 
	{	
		CalculatorModel calcModel = new CalculatorModel();	
		CalculatorView calcView = new CalculatorView( calcModel );
		CalculatorController calcController = new CalculatorController( calcModel, calcView );
		
		NumberButtonsModel nrBttnsModel = new NumberButtonsModel();
		NumberButtonsView nrBttnsView = new NumberButtonsView( nrBttnsModel );
				
		OperatorButtonsModel opBttnsModel = new OperatorButtonsModel();
		OperatorButtonsView opBttnsView = new OperatorButtonsView( opBttnsModel );
		
		calcController.registerButtons( nrBttnsModel );
		calcController.registerButtons( opBttnsModel );
		
		JPanel buttonsPanel = createButtonsPanel( nrBttnsView, opBttnsView );
		JPanel rootPanel = createRootPanel( calcView, buttonsPanel );
		
		JFrame frame = createFrame( rootPanel );
		frame.setVisible( true );
	}
	
	/**
	 * Create the frame for the calculator application.
	 * 
	 * @param rootPanel the root panel for the calculator views.
	 * 
	 * @return the frame for the calculator application.
	 */
	private static JFrame createFrame( JPanel rootPanel )
	{
		JFrame frame = new JFrame();
		frame.setSize( 800, 550 );
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		frame.setTitle( "WagterCalc" );
		frame.setResizable( false );
		frame.setContentPane( rootPanel );
		
		return frame;
	}
	
	/**
	 * Create the root panel for the calculator views.
	 * 
	 * @param calcView the calculator view (display).
	 * @param buttonsPanel the panel containing the button views.
	 * 
	 * @return the root panel for the calculator views.
	 */
	private static JPanel createRootPanel( CalculatorView calcView, JPanel buttonsPanel )
	{
		JPanel rootPanel = new JPanel();
		rootPanel.setLayout( new BoxLayout( rootPanel, BoxLayout.Y_AXIS ));
		rootPanel.add( calcView );
		rootPanel.add( buttonsPanel );
		
		return rootPanel;
	}
	
	/**
	 * Create the panel for the button views.
	 * 
	 * @param nrBttnsView the number button view.
	 * @param opBttnsView the operator buttons view.
	 * 
	 * @return the panel containing the button views.
	 */
	private static JPanel createButtonsPanel( NumberButtonsView nrBttnsView, OperatorButtonsView opBttnsView )
	{	
		JPanel innerPanel = new JPanel();
		innerPanel.setLayout( new BoxLayout(innerPanel, BoxLayout.X_AXIS) );
		
		// add the buttons views to the panel 
		innerPanel.add( nrBttnsView );
		innerPanel.add( Box.createRigidArea( new Dimension(80,1) ) );
		innerPanel.add( opBttnsView );
				
		JPanel outerPanel = new JPanel();
		outerPanel.add( innerPanel );
		
		return outerPanel;
	}
}
