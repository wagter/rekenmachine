package wagtercalc.view;

import wagtercalc.model.CalculatorModel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

public class CalculatorView extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	/**
	 * The calculator model.
	 */
	private CalculatorModel calcModel;
	
	/**
	 * CalculatorView class constructor
	 * 
	 * @param model the CalculatorModel 
	 */
	public CalculatorView( CalculatorModel model )
	{
		calcModel = model;
		setPreferredSize( new Dimension(800, 80) );
	}
	
	/**
	 * Set the CalculatorModel instance.
	 * 
	 * @param calcModel the CalculatorModel instance.
	 */
	public void setModel( CalculatorModel calcModel )
	{
		this.calcModel = calcModel;
	}
	
	@Override
	public void paintComponent( Graphics g )
	{
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
		
		super.paintComponent(g2d);
		g2d.setColor( new Color( 33, 33, 33 ) );
		g2d.fillRoundRect( 30, 30, 740, 70, 15, 15 );
		
		g2d.setColor( Color.WHITE );
		g2d.setFont( new Font("Tlwg Mono Bold", Font.PLAIN, 22) );
		
		// don't display the first character, as it is a prefixed zero.
		g2d.drawString( calcModel.toString().substring(1), 55, 72 );
	}
}
