/**
 * 
 */
package wagtercalc.controller;

import wagtercalc.helper.Calculator;

import wagtercalc.CustomButton;
import wagtercalc.model.AbstractButtonsModel;
import wagtercalc.model.CalculatorModel;
import wagtercalc.view.CalculatorView;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

import java.util.HashMap;
import java.util.Map;

import javax.swing.event.MouseInputAdapter;

/**
 * @author Joris Wagter
 *
 */
public class CalculatorController
{
	/**
	 * The calculator model.
	 */
	private CalculatorModel calcModel;
	
	/**
	 * The calculator view.
	 */
	private CalculatorView calcView;
	
	/**
	 * The amount of keyboard keys that are pressed.
	 */
	private int keyboardPressed;
	
	/**
	 * A HashMap to keep a history of CalculatorModel instances.
	 */
	private HashMap<Integer, CalculatorModel> historyMap;
	
	
	/**
	 * The Calculator Controller class constructor.
	 * 
	 * @param cm the model
	 * @param cv the view
	 */
	public CalculatorController( CalculatorModel cm, CalculatorView cv )
	{
		calcModel = cm;
		calcView = cv;
		
		historyMap = new HashMap<Integer, CalculatorModel>();
		
		initKeyBoardListener();
	}
	
	/**
	 * Register buttons for characters. 
	 * 
	 * @param btnsModel the model containing the buttons.
	 */
	public void registerButtons( AbstractButtonsModel btnsModel )
	{
		CustomButtonListener listener = new CustomButtonListener();
		
		for ( Map.Entry<Character, CustomButton> entry : btnsModel.getButtonMap().entrySet() ) {
	    	CustomButton btn = entry.getValue();
	    	if ( !btn.isPlaceholder() )
				btn.addMouseListener( listener );
		}
	}
	
	/**
	 * Handle the character of a pressed key. 
	 * 
	 * When a key on the keyboard is pressed, the event fires 3 times.
	 * To prevent 3 instead of 1 characters to be added, set the second 
	 * argument to true Then the method will ignore two triggers.
	 * 
	 * @param c the character of the pressed key.
	 * @param isKeyboard if the key is from a keyboard.
	 */
	private void keyPressed( char c , boolean isKeyboard )
	{
		if ( isKeyboard ) keyboardPressed++;
		if ( isKeyboard && keyboardPressed % 3 != 0 ) return;
		
		if ( Calculator.isNumeric( c ) || Calculator.isOperator( c ) ) {	
			
			// If the current model contains an answer, create a new model with the current answer.
			if ( calcModel.getAnswer() != null ) newModelFromAnswer();
			
			// Add character to model and repaint the view.
			Calculator.addChar( calcModel, c );	
			calcView.repaint();
			
		} else {
			
			switch (c) {
			case 'd':
				// Delete: remove the last character from the model.
				Calculator.removeChar( calcModel );
				calcView.repaint();
				break;
			case 'c':
				// Clear: create a new empty model.
				calcModel = new CalculatorModel();
				calcView.setModel( calcModel );
				calcView.repaint();
				break;
			case '=':
				// Equals: calculate the model's expression and set the answer.
				Calculator.calculate( calcModel );
				calcView.repaint();
			}
			
		}

	}
	
	/**
	 * Create a new model from the current answer.
	 */
	private void newModelFromAnswer()
	{
		
		historyMap.put( 
			historyMap.size(), 
			new CalculatorModel( calcModel.getExpressionMap(), calcModel.getAnswer() ) 
		);
			
		calcModel = new CalculatorModel( calcModel.getAnswer() );
		calcView.setModel( calcModel );
	}
	
	/**
	 * A mouse listener class to handle button events.
	 */
	private class CustomButtonListener extends MouseInputAdapter
    {
        @Override
        public void mousePressed( MouseEvent me )
        {
            CustomButton btn = (CustomButton) me.getSource();
            char c = btn.getCharacter();
            keyPressed( c, false );
        }
        
        @Override
        public void mouseEntered(MouseEvent me) 
        {
        	CustomButton btn = (CustomButton) me.getSource();
        	btn.setHover( true );
        }
        
        @Override
        public void mouseExited(MouseEvent me) 
        {
        	CustomButton btn = (CustomButton) me.getSource();
        	btn.setHover( false );
        }
        
        
    }
	
	/**
	 * Initializes the keyboard listener
	 */
	private void initKeyBoardListener()
	{
		KeyboardFocusManager.getCurrentKeyboardFocusManager()
		  .addKeyEventDispatcher( new KeyEventDispatcher() {
		      @Override
		      public boolean dispatchKeyEvent( KeyEvent e ) {
		    	  keyPressed( e.getKeyChar() , true );
		    	  return false;
		      }
		});
	}

}
