package wagtercalc.model;

import java.util.HashMap;

import wagtercalc.CustomButton;

/**
 * Interface to be sure children of AbstractButtonsModel have the method "createButtonMap()" implemented.
 * 
 * @author Joris Wagter
 *
 */
public interface ButtonsModelInterface 
{
	public HashMap<Character, CustomButton> createButtonMap();
}
