package wagtercalc.view;

import wagtercalc.model.OperatorButtonsModel;

import java.awt.Color;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;

public class OperatorButtonsView extends AbstractButtonsView 
{
	private static final long serialVersionUID = 1L;
	
	public OperatorButtonsView( OperatorButtonsModel m ) 
	{
		super(m);
	}
	
	@Override
	public void initLayout()
	{
		add( createCol1() );
		add( Box.createRigidArea( hSpacer ) );
		add( createCol2() );
	}
	
	private JPanel createCol1()
	{
		JPanel col = new JPanel();
		col.setLayout( new BoxLayout(col, BoxLayout.Y_AXIS) );
		
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton('d').setColors(new Color( 216, 67, 21 ), new Color( 191, 54, 12 ), Color.WHITE) );
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton('+') );
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton('*') );
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton(')') );
		col.add( Box.createRigidArea( vSpacer ) );
		
		return col;
	}
	
	private JPanel createCol2()
	{
		JPanel col = new JPanel();
		col.setLayout( new BoxLayout(col, BoxLayout.Y_AXIS) );
		
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton('c').setColors(new Color( 198, 40, 40 ), new Color( 183 ,28, 28 ), Color.WHITE) );
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton('-') );
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton('/') );
		col.add( Box.createRigidArea( vSpacer ) );
		col.add( model.getButton('=').setColors(new Color( 46, 125, 50 ), new Color( 27, 94, 32 ), Color.WHITE) );
		col.add( Box.createRigidArea( vSpacer ) );
		
		return col;
	}

}
