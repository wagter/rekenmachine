package wagtercalc.view;

import wagtercalc.model.AbstractButtonsModel;

import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

/**
 * An abstract class to create a custom grid for buttons, created from 
 * a "multidimensional BoxLayout". Basically a horizontal BoxLayout,
 * containing multiple vertical BoxLayouts. 
 * 
 * To create a layout, extend this class and add the 
 * vertical BoxLayout panels in the "initLayout()" method.
 * 
 * @author Joris Wagter
 *
 */
public abstract class AbstractButtonsView extends JPanel implements ButtonsViewInterface
{
	private static final long serialVersionUID = 1L;
	
	/**
	 * Model containing the buttons.
	 */
	protected AbstractButtonsModel model;
	
	/**
	 * Dimension for vertical spacing
	 */
	protected Dimension vSpacer = new Dimension( 5, 10 );
	
	/**
	 * Dimension for horizontal spacing
	 */
	protected Dimension hSpacer = new Dimension( 10, 5 );
	
	/**
	 * AbstractButtonsView class constructor.
	 * 
	 * @param model an AbstractButtonsModel instance.
	 */
	public AbstractButtonsView( AbstractButtonsModel model )
	{
		this.model = model;		
		setLayout( new BoxLayout(this, BoxLayout.X_AXIS) );
		initLayout();
	}
	
	/**
	 * Initialize the layout.
	 * 
	 * Override this method in a child class and use it to add vertical BoxLayout panels.
	 */
	public void initLayout()
	{}

}
