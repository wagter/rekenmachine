package wagtercalc.helper;

import wagtercalc.model.CalculatorModel;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParsePosition;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * This is where the magic happens. The first two methods are the most
 * important. They do the actual calculation of the expression of the 
 * model. The rest are mostly simple helper methods.
 * 
 * @author Joris Wagter
 *
 */
public class Calculator 
{	
	/**
	 * Calculate the expression and replace it with the result.
	 * 
	 * In the comments I use the expression "2 + 5 * 3" as example.
	 * 
	 * @param calcModel the instance of the calculator model.
	 */
	public static void calculate( CalculatorModel calcModel )
	{	
		String answer = null;
		
		// Create a new HashMap from the model's expressionMap. It now looks like this: { 0="02", 1="+", 2="5", 3="*", 4="3" }.
		HashMap<Integer, String> calcMap = new HashMap<Integer, String>( calcModel.getExpressionMap() );
		
		// Get a string representing the expression to check for operators.
		String calcString = calcModel.toString();
		
		if ( calcString.contains("*") || calcString.contains("/")  ) {		
			// Loop through the calculation HashMap entries and look for the operators for multiplication and division.  
		    for ( Map.Entry<Integer, String> entry : calcMap.entrySet() ) {
		    	String value = entry.getValue();
		    	if ( value.equals("*") || value.equals("/") ) {
		    		int operatorKey = entry.getKey();
		    		calculateSingle( calcMap, operatorKey );	    	
		    	}
		    }    
		}
		
		// The HashMap now looks like this: { 0="02", 1="+", 2=null, 3=null, 4="15" }.
	    
	    if ( calcString.contains("+") || calcString.contains("-") ) { 
			// loop through the calculation HashMap entries and look for the operators for adding and subtraction. 
	    	for ( Map.Entry<Integer, String> entry : calcMap.entrySet() ) {
		    	String value = entry.getValue();
		    	if ( value != null && ( value.equals("+") || value.equals("-") ) ) {
		    		int operatorKey = entry.getKey();
		    		calculateSingle( calcMap, operatorKey );	    	
		    	}
		    }
	    }
	    
	    // The HashMap now looks like this: { 0=null, 1=null, 2=null, 3=null, 4="17" }.

	    // Select the final entry, containing the answer.
	    String finalEntry = calcMap.get( calcMap.size() - 1 );
	    
	    // Parse answer into correct format and set in model.
	    if ( !isOperator( finalEntry.charAt( finalEntry.length() - 1 ) ) ) {
	    	
	    	BigDecimal bigAnswer = new BigDecimal(finalEntry); 	
	    	// base answer string format on the check if bigAnser has an integer value. 
	    	String format = (bigAnswer.signum() == 0 || bigAnswer.scale() <= 0 || bigAnswer.stripTrailingZeros().scale() <= 0) ? "%.0f" : "%.1f";
	    	answer = String.format( Locale.GERMANY, format, bigAnswer );
	    }
	    
	    calcModel.setAnswer( answer );    
	}
	
	/**
	 * Calculate a single expression in a HashMap.
	 * 
	 * @param map the HashMap containing the expression.
	 * @param operatorKey the key of the operator of the expression.
	 */
	private static void calculateSingle( HashMap<Integer, String> map, Integer operatorKey ) 
	{
		// get the correct index of the values by checking for null values and incrementing offset.
		int leftIndex = 1;
		while ( operatorKey - leftIndex >= 0 && map.get( operatorKey - leftIndex  ) == null )
			leftIndex++;
		String leftVal = map.get( operatorKey - leftIndex  );
		
		int rightIndex = 1;	
		while ( operatorKey + rightIndex < map.size() && map.get( operatorKey + rightIndex  ) == null )
			rightIndex++;		
		String rightVal = map.get( operatorKey + rightIndex  );
		
		// check if the value of the operatorKey is actually an operator.
		// and check if the values to the left and right of the operator are not null.
		if ( isOperator(map.get(operatorKey).charAt(0)) && leftVal != null && rightVal != null ) {
			
			char operator = map.get( operatorKey ).charAt(0);
			
			// select entries to the left and right of the operator and parse them into a BigDecimal.
			DecimalFormat df = getDecimalFormat();
			BigDecimal a = (BigDecimal) df.parse( leftVal, new ParsePosition(0) );
			BigDecimal b = (BigDecimal) df.parse( rightVal, new ParsePosition(0) );
			
			// set default value for answer to zero
			BigDecimal x = new BigDecimal( 0 );
			
			// calculate the expression.
			switch ( operator ) {
	    	case '+':
	    		x = a.add( b );
	    		break;
	    	case '-':
	    		x = a.subtract( b );
	    		break;
	    	case '*':
	    		x = a.multiply( b );
	    		break;
	    	case '/':
	    		// check if a or b is not equal to the default answer (zero) to prevent division by zero exception.
	    		if ( a.compareTo( x ) != 0 && b.compareTo( x ) != 0 )
	    			// set precision and rounding mode. See answer from:
	    			// https://stackoverflow.com/questions/4591206/arithmeticexception-non-terminating-decimal-expansion-no-exact-representable
	    			x = a.divide( b, 1, RoundingMode.HALF_UP );
	    		break;
	    	}
			
			// Set the operator entry to null.
			map.put( operatorKey, null );
	    	// Put the result in the place of the value at the right of the operator.
	    	map.put( operatorKey + rightIndex, "" + x );
	    	// Set the value at the left of the operator to null.
	    	if ( operatorKey > 0 ) map.put( operatorKey - leftIndex, null );
		}
		
	}
	
	private static DecimalFormat getDecimalFormat()
	{
		DecimalFormat df;
		DecimalFormatSymbols symbols = new DecimalFormatSymbols();
		symbols.setGroupingSeparator( '.' );
		symbols.setDecimalSeparator( ',' );
		df = new DecimalFormat( "#,##0.0#", symbols );
		df.setParseBigDecimal( true );
		
		return df;
	}
	
	/**
	 * Add a character to the model.
	 * 
	 * @param calcModel the instance of the calculator model.
	 * @param c the character to add to the model.
	 */
	public static void addChar( CalculatorModel calcModel, char c )
	{
		HashMap<Integer, String> expressionMap = calcModel.getExpressionMap();
		
		int length = getLength( calcModel );
		Character lastChar 		 = length - 1 >= 0 ? getCharAt( calcModel, length - 1 ) : null;
		Character secondLastChar = length - 2 >= 0 ? getCharAt( calcModel, length - 2 ) : null;
		
		if ( isNumeric(c) ) {
			// if there are two operators in a row, the last operator must be a '-'. in this case the '-' 
			// needs to be treated as a number for negative number notation.
			if ( isOperator(lastChar) && !isOperator(secondLastChar) ) {		
				expressionMap.put( expressionMap.size(), "" + c );
			} else {
				int index = expressionMap.size() - 1;
				String str = expressionMap.get( index ) != null ? expressionMap.get( index ) : "";
				expressionMap.put(index, str + c );
			}			
		}
		
		if ( isOperator( c ) ) {
			// if the last character of the expression list is an operator,
			// prevent it from being added, unless the character to be added is a '-'.
			// for negative number notation.
			if ( isOperator( lastChar ) && c == '-' ) {
				expressionMap.put( expressionMap.size(), "" + c );			
			} else {
				expressionMap.put( expressionMap.size(), "" + c );
			}			
		}
		
	}
	
	/**
	 * Remove the last character from the model.
	 * 
	 * @param calcModel the instance of the calculator model.
	 */
	static public void removeChar( CalculatorModel calcModel )
	{	
		HashMap<Integer, String> expressionMap = calcModel.getExpressionMap();
		
		// get the index of the last expression map entry and the index of the last character of the entry.
		int lastEntryIndex = expressionMap.size() - 1;
		int lastCharIndex = expressionMap.get( lastEntryIndex ).length() - 1;
		
		if ( lastCharIndex == 0 && lastEntryIndex != 0 ) {
			expressionMap.remove( lastEntryIndex );
		} else {
			if ( (lastEntryIndex == 0 && lastCharIndex >= 1) || (lastEntryIndex > 0 && lastCharIndex > 0) ) {
				String mapStr = expressionMap.get( lastEntryIndex ).substring( 0, lastCharIndex );
				expressionMap.put( lastEntryIndex, mapStr );
			}
		}
	}
	
	/**
	 * Get the second last character of the expression map of the calculator model.
	 * 
	 * @param calcModel the instance of the calculator model.
	 * @param pos the position of the character position
	 * @return the character of the position.
	 */
	public static Character getCharAt( CalculatorModel calcModel, int pos )
	{	
		String str = calcModel.toCompactString();
		return str.length() > pos ? str.charAt( pos ) : null;
	}
	
	/**
	 * Get the length of the string representation of the calculator model.
	 * 
	 * @param calcModel the instance of the calculator model.
	 * @return the character of the position.
	 */
	public static int getLength( CalculatorModel calcModel )
	{
		return calcModel.toCompactString().length();
	}
	
	/**
	 * Check if a character is a valid operator.
	 * 
	 * @param c the character
	 * @return if the character is a valid operator.
	 */
	public static boolean isOperator( char c )
	{
		return "+-*/".indexOf( "" + c ) >= 0;
	}
	
	/**
	 * Check if a character is a valid number.
	 * 
	 * @param c the character
	 * @return if the character is a valid number.
	 */
	public static boolean isNumeric( char c ) 
	{
		return "1234567890,".indexOf( "" + c ) >= 0;
	}

}
