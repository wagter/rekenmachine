package wagtercalc;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;

import javax.swing.JPanel;

public class CustomButton extends JPanel 
{

	private static final long serialVersionUID = 1L;
	
	/**
	 * The character of the button.
	 */
	private char character;
	
	/**
	 * The background color of the button.
	 */
	private Color clrBg;
	
	/**
	 * The background color of the button when in hover state.
	 */
	private Color clrBgHover;
	
	/**
	 * The color of the character of the button.
	 */
	private Color clrTxt;
	
	/**
	 * The hover state of the button.
	 */
	private boolean hover = false;
	
	/**
	 * If the button is a placeholder.
	 */
	private boolean placeholder = false;
	
	/**
	 * CustomButton class constructor. 
	 * 
	 * If the character is a space, the button will be set to the
	 * placeholder state. No button will be drawn and the state
	 * can be used to ignore mouse events. 
	 * 
	 * @param c the character of the button.
	 */
	public CustomButton( char c )
	{
		super();
		placeholder = c == ' ';
		character = c;
		
		setPreferredSize( new Dimension(97,60) );
		
		clrBg = new Color( 55, 71, 79 );
		clrBgHover = new Color( 38, 50, 56 );
		clrTxt = Color.WHITE;
		
		if ( !placeholder ) 
			setCursor( Cursor.getPredefinedCursor(Cursor.HAND_CURSOR) );
	}
	
	/**
	 * Set the colors of the button if they have to be different from the default.
	 * 
	 * @param clrBg the background color.
	 * @param clrBgHover the background color for the hover state.
	 * @param clrTxt the color of the character.
	 * 
	 * @return return self for method chaining.
	 */
	public CustomButton setColors( Color clrBg, Color clrBgHover, Color clrTxt )
	{
		this.clrBg = clrBg;
		this.clrBgHover = clrBgHover;
		this.clrTxt = clrTxt;
		
		return this;
	}
	
	/**
	 * Get the character of the button.
	 * 
	 * @return the character of the button.
	 */
	public char getCharacter()
	{
		return character;
	}
	
	/**
	 * Set the character of the button.
	 * 
	 * @param c the character to set.
	 */
	public void setCharacter( char c )
	{
		character = c;
		repaint();
	}
	
	/**
	 * Set the hover state of the button.
	 * 
	 * @param hover the boolean for the hover state.
	 */
	public void setHover( boolean hover ) 
	{
		this.hover = hover;
		repaint();
	}
	
	/**
	 * Check if the button is a placeholder.
	 * 
	 * @return if the button is a placeholder.
	 */
	public boolean isPlaceholder()
	{
		return placeholder;
	}
	
	@Override
	public void paintComponent( Graphics g )
	{
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
		
		super.paintComponent(g2d);
		
		if ( placeholder ) return;
		
		g2d.setColor( hover ? clrBgHover : clrBg );
		g2d.fillRoundRect( 0, 0, 97, 60 , 15, 15);
			
		g2d.setColor( clrTxt );
		g2d.setFont( new Font("Tlwg Mono Bold", Font.BOLD, 20) );
		g2d.drawString( "" + character, 42, 37 );
	}
}
